package com.test.pihadoop;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;

import java.io.IOException;

public class PiRecordReader extends RecordReader<Text, PiTaskParams> {

    private PiTaskParams params;
    private boolean toggle = false;

    public PiRecordReader (PiInputSplit inputSplit) {
        this.params = new PiTaskParams(inputSplit.getStart(), inputSplit.getEnd(), inputSplit.getTaskNo());
    }

    public void initialize(InputSplit inputSplit, TaskAttemptContext taskAttemptContext) throws IOException, InterruptedException {

    }

    public boolean nextKeyValue() throws IOException, InterruptedException {
        toggle = !toggle;
        return toggle;
    }

    public Text getCurrentKey() throws IOException, InterruptedException {
        return new Text("Task: " + params.getTaskNum());
    }

    public PiTaskParams getCurrentValue() throws IOException, InterruptedException {
        return params;
    }

    public float getProgress() throws IOException, InterruptedException {
        return 0;
    }

    public void close() throws IOException {

    }
}
