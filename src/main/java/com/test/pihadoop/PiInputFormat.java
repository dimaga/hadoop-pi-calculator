package com.test.pihadoop;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PiInputFormat extends InputFormat<PiTaskParams, NullWritable> {

    private static final long numOfIterations = 453271471;
    private static final long numOfPartitions = 10;

    public List<InputSplit> getSplits(JobContext jobContext) throws IOException, InterruptedException {
        List<InputSplit> splits = new ArrayList<InputSplit>();

        long step = numOfIterations / numOfPartitions;
        for (int i = 0; i < numOfPartitions; i++) {
            long startIteration = i * step;
            long endIteration = (i + 1) * step;

            if(i == numOfPartitions - 1) {
                endIteration = numOfIterations;
            }

            splits.add(new PiInputSplit(startIteration, endIteration, i));
        }

        return splits;
    }

    public RecordReader createRecordReader(InputSplit inputSplit, TaskAttemptContext taskAttemptContext) throws IOException, InterruptedException {
        return new PiRecordReader((PiInputSplit) inputSplit);
    }
}
