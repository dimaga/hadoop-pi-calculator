package com.test.pihadoop;

import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.output.NullOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class Main extends Configured implements Tool{

    public static void main(String... args) throws Exception {
        Main m = new Main();
        ToolRunner.run(m, args);
    }

    public int run(String[] strings) throws Exception {
        Job job = new Job();
        job.setJarByClass(Main.class);
        job.setJobName("Mega job to calculate Pi number");

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(PiTaskResult.class);

        job.setInputFormatClass(PiInputFormat.class);
        job.setOutputFormatClass(NullOutputFormat.class);
        job.setMapperClass(PiMapper.class);
        job.setReducerClass(PiReducer.class);

        job.waitForCompletion(false);

        return 0;
    }
}
