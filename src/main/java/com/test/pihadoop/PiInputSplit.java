package com.test.pihadoop;

import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.InputSplit;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class PiInputSplit extends InputSplit implements Writable, Comparable {

    private long start;
    private long end;
    private int taskNo;

    public  PiInputSplit() {}

    public PiInputSplit(long start, long end, int taskNo) {
        this.start = start;
        this.end = end;
        this.taskNo = taskNo;

    }

    public long getStart() {
        return start;
    }

    public long getEnd() {
        return end;
    }

    public int getTaskNo() {
        return taskNo;
    }

    public long getLength() throws IOException, InterruptedException {
        return 0;
    }

    public String[] getLocations() throws IOException, InterruptedException {
        return new String[0];
    }

    public int compareTo(Object o) {
        return this.taskNo - ((PiInputSplit)o).getTaskNo();
    }

    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeLong(this.start);
        dataOutput.writeLong(this.end);
        dataOutput.writeInt(this.taskNo);
    }

    public void readFields(DataInput dataInput) throws IOException {
        this.start = dataInput.readLong();
        this.end = dataInput.readLong();
        this.taskNo = dataInput.readInt();
    }
}
