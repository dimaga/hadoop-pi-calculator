package com.test.pihadoop;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.math.BigDecimal;

public class PiMapper extends Mapper<Text, PiTaskParams, Text, PiTaskResult> {

    public static final Text PARTIAL_RESULT = new Text("PARTIAL_RESULT");

    @Override
    protected void map(Text key, PiTaskParams params, Context context) throws IOException, InterruptedException {

        System.out.println("Processing task: " + key.toString() + ", by mapper: " + context.getTaskAttemptID());

        PiCalculator calculator = new PiCalculator();
        BigDecimal result = calculator.calculatePi(params.getStartIter(), params.getEndIter());

        PiTaskResult taskResult = new PiTaskResult(result);

        context.write(PARTIAL_RESULT, taskResult);
    }
}
