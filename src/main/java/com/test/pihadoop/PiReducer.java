package com.test.pihadoop;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Iterator;

public class PiReducer extends Reducer<Text, PiTaskResult, Text, Text> {

    private static final Text TOTAL_SUM = new Text("TOTAL SUM");
    @Override
    protected void reduce(Text key, Iterable<PiTaskResult> values, Context context) throws IOException, InterruptedException {
        System.out.println("Reduce for key: " + key.toString());
        BigDecimal sum = new BigDecimal(0);

        Iterator iterator = values.iterator();

        int valuesCounter = 0;
        while (iterator.hasNext()) {
            PiTaskResult nextRes = (PiTaskResult)iterator.next();
            sum = sum.add(nextRes.getResult());
            valuesCounter++;
        }

        System.out.println("Total num of items from mappers: " + valuesCounter);
        System.out.println("Total sum: " + sum.toString());
        context.write(TOTAL_SUM, new Text(sum.toString()));
    }
}
