package com.test.pihadoop;

public class PiTaskParams {
    private long startIter;
    private long endIter;
    private int taskNum;

    public PiTaskParams(long startIter, long endIter, int taskNum) {
        this.startIter = startIter;
        this.endIter = endIter;
        this.taskNum = taskNum;
    }

    public int getTaskNum() {
        return taskNum;
    }

    public long getStartIter() {
        return startIter;
    }

    public long getEndIter() {
        return endIter;
    }
}
