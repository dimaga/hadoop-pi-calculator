package com.test.pihadoop;

import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.math.BigDecimal;

public class PiTaskResult implements Writable{

    private BigDecimal result;

    public PiTaskResult (BigDecimal result) {
        this.result = result;
    }

    public BigDecimal getResult() {
        return result;
    }

    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeUTF(result.toPlainString());
    }

    public void readFields(DataInput dataInput) throws IOException {
        this.result = new BigDecimal(dataInput.readUTF());
    }
}
