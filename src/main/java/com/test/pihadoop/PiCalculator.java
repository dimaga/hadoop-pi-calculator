package com.test.pihadoop;

import java.math.BigDecimal;

import static java.math.BigDecimal.ROUND_HALF_UP;

public class PiCalculator {

//    3.141592653589793238462643383279502884197169399375105820974944592307816406286208998628034825342117067982148086513

    private static final int SCALE = 20;

    public BigDecimal calculatePi(long fromIteration, long toIteration) {
        BigDecimal result = new BigDecimal(0.0);
        BigDecimal divisor;

        for (long i = fromIteration; i < toIteration; i++) {
            divisor = new BigDecimal(1).add(new BigDecimal(i*2));
            if (i % 2 == 0) {
                result = result.add(new BigDecimal(4.0).divide(divisor, SCALE, ROUND_HALF_UP));
            } else {
                result = result.subtract(new BigDecimal(4.0).divide(divisor, SCALE, ROUND_HALF_UP));
            }
        }

        return result;
    }
}
