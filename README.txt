Implementation of distributed PI calculation using Apache hadoop. I think hadoop is good idea for big amount
of calculations. Currently in PiInputFormat hardcoded number of parts, to split total number of iterations.
PiInputFormat and PiInputSplit are responsible for reading input data and separate it in chunks (or splits). Each split
will be processed by separate mapper task.
When running locally only one mapper will process all tasks, so technically it's not fully completed task :). To have three
worker nodes cluster from 3 nodes required. Also may required extra tools for better monitoring execution process. But
it may be a good solution for big amount of data.